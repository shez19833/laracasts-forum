@component('profiles.activities.activity')

    @slot('heading')
        {{ $activity->user->name }} published <a href="{{ $activity->subject->path() }}">{{ $activity->subject->title }}</a>
    @endslot

    @slot('date')
        {{ $activity->created_at->diffForHumans() }}
    @endslot

    @slot('body')
        {{ $activity->subject->body }}
    @endslot

@endcomponent
