@extends('layouts.app')


@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 justify-content-center">
                <div class="page-header">
                    <h1>
                        {{ $user->name }} <smaller>registered: {{ $user->created_at->diffForHumans() }}</smaller>
                    </h1>
                </div>

                <div class="container">
                    <div class="row">
                        <div>
                            @foreach($activities as $date => $activity)
                                <h3 class="page-header">{{ $date }}</h3>
                                @foreach($activity as $item)
                                    @include("profiles.activities.{$item->type}", ['activity' => $item])
                                @endforeach
                            @endforeach

                            {{--{{ $activities->links() }}/--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection