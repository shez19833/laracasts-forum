<div class="card mt-4">
    <div class="card-header">
        <div class="level">
            <div class="flex">
                <a href="{{ route('profile.show', $reply->owner) }}">{{ $reply->owner->name }}</a> said {{ $reply->created_at->diffForHumans() }}
            </div>

            <div>
                <form method="post" action="{{ route('favourite.store', ['id' => $reply->id]) }}">
                    {{ csrf_field() }}
                    <button class="btn btn-outline-secondary" type="submit" name="Favourite" {{ $reply->isFavourited() ? 'disabled' : '' }}>
                        {{ $reply->favourites_count }} {{ str_plural('Favourite', $reply->favourites_count) }}
                    </button>
                </form>
            </div>
        </div>
    </div>

    <div class="card-body">
        {{ $reply->body }}
    </div>
</div>