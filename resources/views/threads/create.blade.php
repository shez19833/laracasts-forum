@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create New Thread</div>

                    <div class="card-body">
                        <form method="post" action="{{ route('threads.store') }}">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <select name="channel_id" class="form-control" required>
                                    <option value="">..Choose a channel</option>

                                    @foreach($channels as $channel)
                                        <option value="{{ $channel->id }}" {{ old('channel_id') == $channel->id ? 'selected' : '' }}>
                                            {{ $channel->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <input class="form-control" name="title" placeholder="Add Title" value="{{ old('title') }}" required/>
                            </div>

                            <div class="form-group">
                                <textarea name="body" class="form-control" placeholder="Add Body" rows="5" required>{{ old('body') }}</textarea>
                            </div>

                            <button type="submit" class="btn btn-outline-primary">Post</button>

                            @if ( count($errors) )
                                <ul class="alert alert-danger mt-3">
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
