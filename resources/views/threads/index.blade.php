@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">All Threads <a href="{{ route('threads.create') }}">Create New Thread</a> </div>

                    <div class="card-body">
                        @foreach($threads as $thread)
                              <article>
                                  <div class="level">
                                    <h3 class="flex"><a href="{{ $thread->path() }}">{{ $thread->title }}</a></h3>
                                    <a href="{{ $thread->path() }}">{{ $thread->replies_count }} {{ str_plural('reply', $thread->replies_count) }}</a>
                                  </div>
                                  <div class="body">{{ $thread->body }}</div>
                                  <hr/>
                              </article>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
