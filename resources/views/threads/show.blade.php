@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-md-start">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header level">
                        <span class="flex">
                            <a href="{{ route('profile.show',  $thread->owner) }}">{{ $thread->owner->name }}</a> posted: {{ $thread->title }}
                        </span>

                        @can('delete', $thread)
                        <span>
                            <form method="post" action="{{ $thread->path() }}">
                                @csrf
                                @method('delete')

                                <button class="btn btn-outline-danger" name="delete">Delete</button>
                            </form>
                        </span>
                        @endcan
                    </div>

                    <div class="card-body">
                        {{ $thread->body }}
                    </div>
                </div>

                @foreach($replies as $reply)
                    @include('threads.reply')
                @endforeach

                {{ $replies->links() }}

                @if(auth()->check())
                    <form class="mt-4" method="post" action="{{ $thread->path() . '/replies' }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <textarea name="body" class="form-control" placeholder="Something to add...?" rows="5"></textarea>
                        </div>

                        <button type="submit" class="btn btn-outline-primary">Post</button>
                    </form>
                @endif
            </div>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        Useful Info
                    </div>

                    <div class="card-body">
                        <p>
                            This thread was published {{ $thread->created_at->diffForHumans() }} by
                            <a href="#">{{ $thread->owner->name }}</a> and currently has {{ $thread->replies_count }} {{ str_plural('reply', $thread->replies_count) }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
