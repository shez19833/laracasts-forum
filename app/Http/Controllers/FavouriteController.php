<?php

namespace App\Http\Controllers;

use App\Reply;

class FavouriteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Reply $reply
     * @return void
     */
    public function store(Reply $reply)
    {
        $reply->favourite(\Auth::id());

        return back();
    }
}
