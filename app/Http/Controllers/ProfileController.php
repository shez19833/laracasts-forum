<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Channel;
use App\Filters\ThreadFilters;
use App\Thread;
use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $activities = Activity::feed($user);

        return view('profiles.show', compact('user', 'activities'));
    }
}
