<?php

namespace App\Traits;

use Auth;
use App\Activity;

trait RecordsActivity
{
    //TIP boot and then name of TRAIT
    protected static function bootRecordsActivity()
    {
        if ( auth()->guest() ) return;

        foreach (static::getActivitiesToRecord() as $event) {
            static::$event(function ($model) use ($event) {
                $model->recordActivity($model, $event);
            });
        }

        static::deleting(function($model){
           $model->activity()->delete();
        });
    }

    protected static function getActivitiesToRecord()
    {
        return ['created'];
    }

    protected function recordActivity($model, $event)
    {
        $this->activity()->create([
            'type' => $this->getActivityType($model, $event),
            'user_id' => Auth::id(),
        ]);
    }

    public function activity()
    {
        return $this->morphMany(Activity::class, 'subject');
    }

    protected function getActivityType($model, $event)
    {
        return $event . '_' . strtolower((new \ReflectionClass($model))->getShortName());
    }
}