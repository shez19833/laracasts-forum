<?php

namespace App\Traits;

use App\Favourite;

trait Favouritable
{
    public function getFavouritesCountAttribute()
    {
        return $this->favourites->count();
    }

    public function favourites()
    {
        return $this->morphMany(Favourite::class, 'favourited');
    }

    public function favourite($user_id)
    {
        $attr = ['user_id' => $user_id];

        if (! $this->favourites()->where($attr)->exists() ) {
            $this->favourites()->create($attr);
        }
    }

    public function isFavourited()
    {
        //Tip !! = casting to boolean
        return !! $this->favourites->where('user_id', \Auth::id())->count();
    }
}