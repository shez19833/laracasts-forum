<?php

namespace Tests\Unit;

use App\Thread;
use App\Activity;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ActivityTest extends TestCase
{
    use DatabaseMigrations;

    public function test_activity_recorded_when_thread_created()
    {
        $this->signIn();

        $thread = create(Thread::class);

        $this->assertDatabaseHas('activities', [
            'type' => 'created_thread',
            'user_id' => \Auth::id(),
            'subject_id' => $thread->id,
            'subject_type' => get_class($thread),
        ]);

        $activity = Activity::first();
        $this->assertEquals($activity->subject->id, $thread->id);
    }

    public function test_activity_fetches_feed_for_any_user()
    {
        $this->signIn();

        create(Thread::class, ['user_id' => \Auth::id()], 2);

        \Auth::user()->activity()->first()->update(['created_at' => Carbon::now()->subWeek()]);

        $feed = Activity::feed(\Auth::user());

        $this->assertTrue($feed->keys()->contains(Carbon::now()->format('Y-m-d')));
        $this->assertTrue($feed->keys()->contains(Carbon::now()->subWeek()->format('Y-m-d')));
    }

//    public function test_activity_recorded_when_reply_created()
//    {
//        $this->signIn();
//
//        $reply = create(Reply::class);
//
//
//
//    }
}
