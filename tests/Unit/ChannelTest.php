<?php

namespace Tests\Unit;

use App\Channel;
use App\Thread;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ChannelTest extends TestCase
{
    use DatabaseMigrations;

    public function test_channel_has_threads()
    {
        $channel = create(Channel::class);
        create(Thread::class, ['channel_id' => $channel->id]);

        $this->assertCount(1, $channel->threads);
    }
}
