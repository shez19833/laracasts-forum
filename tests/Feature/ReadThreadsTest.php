<?php

namespace Tests\Feature;

use App\Channel;
use App\Reply;
use App\Thread;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ReadThreadsTest extends TestCase
{
    use DatabaseMigrations;

    private $thread;

    public function setUp()
    {
        parent::setUp();

        $this->thread = create(Thread::class);
    }

    public function test_user_can_see_all_threads()
    {
        $this->get('/threads')
            ->assertStatus(200)
            ->assertSee($this->thread->title);
    }

    public function test_user_can_see_a_threads()
    {
        $this->get($this->thread->path())
            ->assertStatus(200)
            ->assertSee($this->thread->title);
    }

    public function test_user_can_see_replies_associated_with_a_thread()
    {
        $reply = create(Reply::class, [
            'thread_id' => $this->thread->id,
        ]);

        $this->get($this->thread->path())
            ->assertSee($reply->body);
    }

    public function test_user_can_filter_threads_according_to_a_channel()
    {
        $channel = create(Channel::class);
        $thread_in_channel = create(Thread::class, ['channel_id' => $channel->id]);


        $thread_not_in_channel = create(Thread::class);

        $this->get('threads/' . $channel->slug)
            ->assertSee($thread_in_channel->title)
            ->assertDontSee($thread_not_in_channel->title);
    }

    public function test_user_can_filter_threads_by_any_username()
    {
        $this->signIn(create(User::class,['name' => 'JohnDoe']));

        $threads_by_johnDoe = create(Thread::class,['user_id' => \Auth::id()]);
        $threads_not_by_johnDoe = create(Thread::class);

        $this->get('threads?by=JohnDoe')
            ->assertSee($threads_by_johnDoe->title)
            ->assertDontSee($threads_not_by_johnDoe->title);
    }

    public function test_user_can_filter_threads_by_popularity()
    {
        $thread_2_replies = create(Thread::class);
        create(Reply::class, ['thread_id' => $thread_2_replies], 2);

        $thread_3_replies = create(Thread::class);
        create(Reply::class, ['thread_id' => $thread_3_replies], 3);

        $thread_no_replies = $this->thread;

        $response = $this->getJson('threads?popular=1')->json();

        $this->assertEquals([3,2,0], array_column($response, 'replies_count'));
    }
}
