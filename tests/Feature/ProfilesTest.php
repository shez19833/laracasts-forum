<?php

namespace Tests\Feature;

use App\Thread;
use App\User;
use Tests\TestCase;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ProfilesTest extends TestCase
{
    use DatabaseMigrations;

    public function test_a_user_has_a_profile()
    {
        $user = create(User::class);

        $this->get(sprintf('profiles/%s', $user->name))
            ->assertSee($user->name);
    }

    public function test_profile_display_all_threads_by_a_user()
    {
        $this->signIn();

        $thread = create(Thread::class, ['user_id' => \Auth::id()]);

        $this->get(sprintf('profiles/%s', \Auth::user()->name))
            ->assertSee($thread->title);
    }
}
