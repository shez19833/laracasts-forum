<?php

namespace Tests\Feature;

use App\Activity;
use App\Reply;
use App\Thread;
use App\User;
use Tests\TestCase;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class DeleteThreadsTest extends TestCase
{
    use DatabaseMigrations;

    public function test_user_can_delete_their_thread()
    {
        $this->signIn();

        $thread = create(Thread::class, ['user_id' => \Auth::id()]);
        $reply = create(Reply::class, ['thread_id' => $thread->id]);

        $this->delete($thread->path());

        $this->assertDatabaseMissing('threads',  ['id' => $thread->id]);
        $this->assertDatabaseMissing('replies',  ['id' => $reply->id]);

        $this->assertDatabaseMissing('activities',  ['subject_id' => $thread->id, 'subject_type' => get_class($thread)]);
        $this->assertDatabaseMissing('activities',  ['subject_id' => $reply->id, 'subject_type' => get_class($reply)]);
    }

    public function test_guest_users_cannot_delete_a_thread()
    {
        $thread = create(Thread::class);

        $this->delete($thread->path())
            ->assertRedirect('/login');
    }

    public function test_users_cannot_delete_others_thread()
    {
        $thread = create(Thread::class);

        $this->expectException(AuthenticationException::class);

        $this->withoutExceptionHandling()->delete($thread->path());
    }
}
