<?php

namespace Tests\Feature;

use App\Favourite;
use App\Reply;
use Illuminate\Auth\AuthenticationException;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class FavouritesTest extends TestCase
{
    use DatabaseMigrations;

    public function test_unauthenticated_user_cant_favourite_any_reply()
    {
        $this->withExceptionHandling()->post(sprintf('replies/%d/favourites', 1))
            ->assertRedirect('/login');

    }

    public function test_authenticated_user_can_favourite_any_reply()
    {
        $this->signIn();

        $reply = create(Reply::class);

        $this->post(sprintf('replies/%d/favourites', $reply->id));

        $this->assertCount(1, $reply->favourites);
    }

    public function test_authenticated_user_can_favourite_any_reply_once_only()
    {
        $this->signIn();

        $reply = create(Reply::class);

        $this->post(sprintf('replies/%d/favourites', $reply->id));
        $this->post(sprintf('replies/%d/favourites', $reply->id));

        $this->assertCount(1, $reply->favourites);
    }
}
